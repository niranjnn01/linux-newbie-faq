# GROUPS

It is good idea to spend some hours to understand the basics of how groups and permissions work. This is a critical information needed as we progress with linux.

1. when changing ownership of a folder, is it necessary to change the owner ship of the files inside it? wont it automatically happen?


1. When yo use Access Control Lists instead of medling with groups ?


1. What is UMASK of a file / directory ?

https://www.linuxnix.com/umask-define-linuxunix/

1. SUID/SGID/Sticky bit information set of file/ folder ?
https://www.linuxnix.com/suid-set-suid-linuxunix/
https://www.linuxnix.com/sgid-set-sgid-linuxunix/
https://www.linuxnix.com/sticky-bit-set-linux/

1. SUDO, SELinux ?

1. chmod, chown, chgrp ?
