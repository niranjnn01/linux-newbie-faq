
1. Tapping Touchpad is not causing a mouse click in linux

	This functionality is turned off by default
	If you are using GNOME desktop, then it can be enabled from the “mouse and touchpad” settings.

2. WiFi is not working after installing Linux (usually debian)

	As part of debians policy, they will ship only those code which is completely open source and free to use. Many wifi driver codes are not free for use. So debian OS will not ship them by default.

	**/etc/apt/sources.list** is the configuration file that dictates where APT will look for software packages. There are mainly three kinds (main / contributed / non-free ) of packages. You can state explicitly in this file whether to use non-free software.

	If you set it to look for non-free packages as well, then a search for the appropriate WiFi driver will yield result.

	**It is recommended that you familiarize with how package management works. Knowledge of this will help you in installing new software, updating software etc. Also refer to the list of FAQs on installing softwares alone.**

	For a deeper understanding, see this [link](https://wiki.debian.org/SourcesList) . (also available in resources section)

	**NOTE:** *It is not because of any technical limitations that Wifi doesnt work out of the box in debian. It is as a result of a stand they have taken to ship only truley FOS software.*
