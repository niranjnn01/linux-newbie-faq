# Services

A service is an application that can be run in the background, usually in perpetuity in order to perform some task or waiting for requests from other applications to perform tasks.

Source : http://www.lostsaloon.com/technology/how-to-list-all-services-in-linux/

1. How to list all services that are currently running

Most systems keep the startup scripts for the services in the /etc/init.d/ folder. So, a simple listing of the directory contents should show you the list of services that are installed and available to you.

Source : http://www.lostsaloon.com/technology/how-to-list-all-services-in-linux/
