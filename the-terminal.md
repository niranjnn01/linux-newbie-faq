
1. specifying command line arguments are confusing. sometimes arguments are prepended by a "-", sometimes by a "--" and sometimes no dashes ?

	There are 3 ways arguments are specified within a command. Answer to this Stackoverflow Question is very helpful in understanding this. https://unix.stackexchange.com/questions/106847/what-does-aux-mean-in-ps-aux

	From the SO page .

	> Historically, BSD and AT&T developed incompatible versions of ps.
	The options without a leading dash (as per the question) are the BSD style
	while those with a leading dash are AT&T Unix style. On top of this, Linux
	developed a version which supports both styles and then adds to it a third
	style with options that begin with double dashes.



1. The shell is not interactive. many a times, i dont know if i typed a bad command, or the command worked or if it got silently ignored.

	pass -v argument along with your command to make the CLI more interactive. v stands for "verbose"

1. when changing ownership of a folder, is it necessary to change the owner ship of the files inside it? wont it automatically happen?

	To avoid confusion, It is good idea to spend some hours to understand the basics of how groups and permissions work. This is a critical information needed as we progress with linux.
	So there are no easy answers. Resources available under "Permissions and Groups" in the resources section

1. how to copy paste text into the command line ? Cntl + V doesnt work there!

	use Cntl + Shift + V

1. I see Pipe symbol "|" in many commands. what does it do?

1. How to know battery status and details from command line.

	Navigate to /sys/class/power_supply/BAT0/ and use ls command.
	use cat command to see contents of various files displayed.

https://www.cyberciti.biz/faq/linux-laptop-battery-status-temperature/

1. System shutdown and restart

https://www.tldp.org/LDP/lame/LAME/linux-admin-made-easy/system-shutdown-and-restart.html
