# Counter intuitive stuff

1. A read only file can be deleted by a different user.

https://unix.stackexchange.com/questions/94333/whats-the-idea-behind-rm-not-removing-non-writable-file-by-default/94347

1. Why is rm allowed to delete a file under ownership of a different user?

https://unix.stackexchange.com/questions/226563/why-is-rm-allowed-to-delete-a-file-under-ownership-of-a-different-user?rq=1
