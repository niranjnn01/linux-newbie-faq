
# Resources

## General Learning
1. http://www.linfo.org/index.html

## Basics

### Package management

1. APT https://itsfoss.com/apt-command-guide/
2. Difference between APT and APT-GET https://itsfoss.com/apt-vs-apt-get-difference/
2. Sources.list https://wiki.debian.org/SourcesList

### Permissions and Groups

1. https://www.linux.com/learn/understanding-linux-file-permissions
2. https://www.pluralsight.com/blog/it-ops/linux-file-permissions
3. https://help.unc.edu/help/how-to-use-unix-and-linux-file-permissions/

### Processes

1. https://shapeshed.com/unix-ps/#how-to-list-all-processes-by-executable-name
