

1. How to Install software in linux?

  There are multiple ways, each with its pros and cons
  ##### Using the official package management system

  "APT" is the official package management system for Debian based linux. For redhat based distributions, it is DNF.

  **/etc/apt/sources.list** is the configuration file that dictates where APT will look for software packages. There are mainly three kinds (main / contributed / non-free ) of packages. You can state explicitly in this file whether to use non-free software.

  This file also contains the list the URLs of repositories, which the APT is using as source.

    ###### Issues
    1. Not all software are available this way. some popular packages cannot be found using APT or DNF
    2. some packages are community maintained, and they dont perform as well as the official packages. Difficult to differentiate and choose.

  ##### From installation files (.deb / .rpm)

    ###### Issues
    1. Updating and maintaining packages is an issue here. since  there is no package management system doing that work for us.

    For updating, we will have to remove a package and install the latest version.

    In distros like Ubuntu MATE, double clicking the .deb file will start the installation process.(There is a software called package installer which does this?)

  ##### Using PPM maintained by 3rd party.

    ###### Issues
    1. Have to be careful when choosing a 3rd party PPM. reputation and trust of the maintainer has to be ascertained before taking decision to use the package.

  ##### Using 3rd party package management system like
    1. Linuxbrew
    2. NPM
    3. etc
1. what is the difference between apt-get and apt commands?

  apt-get is old, and bit user unfriendly.  While apt is new and more easier to use.
  apt-get is still around. not deprecated. and is used for complex situations that an average linux use wont encounter.

  https://itsfoss.com/apt-vs-apt-get-difference/



1. How to install flash player in linux, for using in firefox browser.

1. In cases where we cannot find packages using apt search (example : "skype"), where they give .deb files for installing software, can we use software like GDebi Package installer (comes with ubuntu MATE). instead of APT, and update it later on etc? what is the opinion?


1. Can i install skype ?
  Yes. You can download the package from skype's official website.
