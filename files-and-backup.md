# Backup

1. Understanding the File system of Linux.

https://freedompenguin.com/articles/quick-tips/linux-file-system-structure-explained/

Notes :
  File System hierarchy(FSH) - maintained by the linux foundation.
  not all distros follow this.


1. How to see contents of files inside the /dev folder?



1. What is difference between hard link and symbolic link?

  https://medium.com/meatandmachines/explaining-the-difference-between-hard-links-symbolic-links-using-bruce-lee-32828832e8d3

  https://medium.com/@wendymayorgasegura/what-is-the-difference-between-a-hard-link-and-a-symbolic-link-8c0493041b62


1. How to take a proper backup of your files. how to verify they were properly backed up?

  There are a number of ways.

  ## Rsync
    Rsync is a utility for efficiently transferring and synchronizing files across computer systems, by checking the timestamp and size of files.
    It is used as the backbone of many advanced backup systems. So it is a good idea to konw more about rsync in detail.

    https://rsync.samba.org/

  ## BackupPC

    BackupPC is a high-performance, enterprise-grade system for backing up Linux, Windows and macOS PCs and laptops to a server's disk.
    https://backuppc.github.io/backuppc/


  ## Timeshift

  https://itsfoss.com/backup-restore-linux-timeshift/
